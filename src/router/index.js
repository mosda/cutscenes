import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import one from '@/pages/one'
import two from '@/pages/tow'

Vue.use(Router)

const router= new Router({
  routes: [
    {
      path: '/',
      name: 'one',
      component: one
    },
    {
      path: '/t',
      name: 'two',
      component: two
    }
  ]
})


export default router;
